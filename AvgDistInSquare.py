import ROOT
import numpy as np

print ''
# Redefine quit to something shorter
def ex():
  quit()
#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(53);
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.15, 'x');
ROOT.gStyle.SetTitleOffset(1.0, 'y');
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(0);  #1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();





#### Given a square of length 1, what is the average distance between any two points inside the square?
# Corners at (0,0), (0,1), (1,0), (1,1)
# Min distance = 0.0, max = sqrt(2)
# But min, max x, y = 0, 1...
# Is this considered Monte Carlo?

# Generate random numbers between 0 and 1 for initial, final x, y positions p times.
p = 10000
x1 = np.random.rand(p)
x2 = np.random.rand(p)
y1 = np.random.rand(p)
y2 = np.random.rand(p)

x = abs(x1-x2)
y = abs(y1-y2)

# Calculate distance between respective points.
r = np.sqrt( x**2 + y**2 )

# Average over all distances.
rAvg = np.mean(r)
print 'Average distance between any two points inside the square:', rAvg
print ''




c = ROOT.TCanvas('c', 'c', 720, 720)
g = ROOT.TGraph(len(x), x, y)
g.GetXaxis().SetLimits(0, 1)
g.GetYaxis().SetRangeUser(0, 1)
g.SetTitle('Average distance, p = '+'{:,}'.format(p)+';;')
#g.SetMarkerStyle(ROOT.kCircle)
g.Draw('ap')


c2 = ROOT.TCanvas('c2', 'c2', 720, 720)
line = ROOT.TLine()
for X1,Y1,X2,Y2 in zip(x1,y1,x2,y2):
  line.SetLineColor(np.random.randint(1,10))
  line.DrawLine(X1,Y1,X2,Y2)


c3 = ROOT.TCanvas('c3', 'c3', 720, 720)
h2 = ROOT.TH2D('h2', 'h2', 100,0,1, 100,0,1)
for X,Y in zip(x,y):
  h2.Fill(X,Y)
h2.Draw('colz')


# This is the only useful visualization. The previous were kind of for fun, or for a more detailed analysis that will never come.
c4 = ROOT.TCanvas('c4', 'c4', 720, 720)
h1 = ROOT.TH1D('h1', 'h1', 100,0,np.sqrt(2))
for R in r:
  h1.Fill(R)
h1.Draw()




c.SaveAs('c.png')
c2.SaveAs('c2.png')
c3.SaveAs('c3.png')
c4.SaveAs('c4.png')

