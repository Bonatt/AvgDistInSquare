### Given a square of length 1, what is the average distance between any two points inside the square?

Let's quickly find out. And I mean quickly; I didn't even label axes!
And because I am uploading this project from a new machine with a different version of ROOT,
I will not rerun my code and mess up the already-existing plots.

I generated 10000 points randomly distributed within a unit square. 
Corners at (0,0), (0,1), (1,0), (1,1). 
So we at least know 0 < r < 1.41.

__Here's a fun plot where I drew a randomly-colored line between two random points:__

![Lines between two random points](c2.png)


__Here's a scatter plot of the x and y distances between two random points:__

![Average distance](c.png)


__And here's a 2D histogram of the above:__

![Averag distance, 2D histogram](c3.png)


__Here's a 1D histogram of the above. The mean of this is 0.522. That is the solution:__

![Average distance, 1D histogram](c4.png)


__But one doesn't need fancy plots. One just needs this:__

```python
# Generate random numbers between 0 and 1 for initial, final x, y positions p times.
p = 10000
x1 = np.random.rand(p)
x2 = np.random.rand(p)
y1 = np.random.rand(p)
y2 = np.random.rand(p)

x = abs(x1-x2)
y = abs(y1-y2)

# Calculate distance between respective points.
r = np.sqrt( x**2 + y**2 )

# Average over all distances.
rAvg = np.mean(r)
print('Average distance between any two points inside the square:', rAvg)
```

```
Average distance between any two points inside the square: 0.5214086211840677
```
